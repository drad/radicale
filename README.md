# README

A docker/helm implementation of [Radicale](https://github.com/Kozea/Radicale) - *A simple CalDAV (calendar) and CardDAV (contact) server. https://radicale.org.*. The primary purpose of this project is to provide an image of Radicale for use in dxc as a calDAV/cardDav server.


### Notices

- the 'data' dir must be owned by 1000:1000: `sudo chown -R 1000:1000 data`


### Implementation Decisions

The project's Dockerfile is used to ensure everything is as close to stock as possible.

We have made the following implementation decisions:

- the `config` file is bundled in the image
- the `user` file will be external
  - in docker deploys will use a volume
  - in k8s deploys will use a secret


### Running

1. set the version of Radicale you want to use by updating `radicale.build.args.VERSION` in `docker-compose.yml`
2. ensure the `config/users` file is set up with the users you need (see Setup Users below for info)
3. start radicale: `du.app` if you are using `ve` otherwise you can use `docker compose up radicale`
4. log in to the [web ui](http://localhost:5232)


### Setup Users

The 'users' file is an apache htpasswd file, you can add/edit lines with the following:

- generate with openssl: `printf "<USER>:$(openssl passwd -apr1 <PASSWORD>)\n" >> config/users`
  + you could also install `apache2-utils` to generate data for htpasswd but we usually have openssl already available
  + __NOTICE: if you start the app as-is it will have a user with username `test` and password `testing`.__


### Updates

This project is designed to build a version of Radicale and run it. Docker Compose is generally used/recommended for local/development instances and k8s/helm for production, see their respective sections below:

- docker compose: update `radicale.build.args.VERSION` in the `docker-compose.yml` file to the version of Radicale you would like
- k8s/helm: update `build.args.VERSION` in the `.ibuilder.toml` file to the version of Radicale you would like
  + use [ib](https://pypi.org/project/ibuilder/) to build and push the image: `ib build -i minor`
  + deploy new version using helm
    - update `version` and `apiVersion` in `Chart.yaml`
  + see k8s/helm section below for more info


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- ensure the `radicale-data` nfs share has been set up (pv and pvc will be created but the share itself needs to be created)
  + __NOTICE:__ the nfs share needs to be owned by `1000:1000`
- deploy the chart: `helm install radicale ./radicale --values radicale/values.yaml.prod --create-namespace --namespace radicale --disable-openapi-validation`
  - update: `helm upgrade radicale radicale/ --values radicale/values.yaml.prod --namespace radicale --disable-openapi-validation`
  - uninstall: `helm uninstall -n radicale radicale`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n radicale deployment radicale`


### Links

- [Radicale GitHub](https://github.com/Kozea/Radicale)
- [Radicale Website](https://radicale.org/)
- [Radicale Docs](https://radicale.org/documentation/)
  - [Basic Setup](https://radicale.org/setup/) - provides authentication info on how to setup a htpasswd file for users/pwds
- [Radicale Dockerfile](https://github.com/Kozea/Radicale/blob/master/Dockerfile)
- [Mailu Radicale Dockerfile](https://hub.docker.com/r/mailu/radicale/dockerfile)
